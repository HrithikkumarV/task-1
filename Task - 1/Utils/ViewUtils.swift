//
//  ViewUtiks.swift
//  Foodzy
//
//  Created by Hrithik Kumar V on 10/05/22.
//

import Cocoa

public enum BorderSide {
    case top, bottom, left, right , all
}

extension NSView {
    
    
    func addBorders(side: BorderSide,
                    color: NSColor,
                    inset: CGFloat = 0.0,
                    thickness: CGFloat = 1.0){

        var borders = [NSView]()

        @discardableResult
        func addBorder(formats: String...) -> NSView {
            let border = NSView()
            border.wantsLayer = true
            border.layer?.backgroundColor = color.cgColor
            border.translatesAutoresizingMaskIntoConstraints = false
            addSubview(border)
            addConstraints(formats.flatMap {
                NSLayoutConstraint.constraints(withVisualFormat: $0,
                                               options: [],
                                               metrics: ["inset": inset, "thickness": thickness],
                                               views: ["border": border]) })
            borders.append(border)
            return border
        }


        if  side == .top || side == .all {
            addBorder(formats: "V:|-0-[border(==thickness)]", "H:|-inset-[border]-inset-|")
        }

        if side == .bottom || side ==  .all {
            addBorder(formats: "V:[border(==thickness)]-0-|", "H:|-inset-[border]-inset-|")
        }

        if side ==  .left || side ==  .all {
            addBorder(formats: "V:|-inset-[border]-inset-|", "H:|-0-[border(==thickness)]")
        }

        if side ==  .right || side ==  .all {
            addBorder(formats: "V:|-inset-[border]-inset-|", "H:[border(==thickness)]-0-|")
        }

        
    }

       
    func addDropShadow() {
        self.layer?.shadowColor = .init(red: 0.6, green: 0.7, blue: 0.8, alpha: 0.9)
        self.layer?.shadowOffset  = CGSize.zero
        self.layer?.shadowOpacity = 0.9
        self.layer?.shadowRadius = 3
        self.layer?.masksToBounds = false
      }
    
}
