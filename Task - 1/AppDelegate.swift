//
//  AppDelegate.swift
//  Task - 1
//
//  Created by Hrithik Kumar V on 22/06/22.
//

import Cocoa

@main
class AppDelegate: NSObject, NSApplicationDelegate {

   
    private var windowController : NSWindowController!
    
    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
        
        openMainWindow()
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }

    func applicationSupportsSecureRestorableState(_ app: NSApplication) -> Bool {
        return true
    }

    private func openMainWindow(){
        let splitViewRouter  : SplitViewRouterProtocol = SplitViewRouter()
        windowController = splitViewRouter.mainWindowController
        windowController.window?.contentViewController = splitViewRouter.baseViewController
        windowController.window?.makeKeyAndOrderFront(nil)
        windowController.window?.makeMain()
        windowController.window?.isReleasedWhenClosed = false
    }
   
    func applicationShouldHandleReopen(_ sender: NSApplication, hasVisibleWindows flag: Bool) -> Bool {
        if !flag{
            windowController.window?.contentViewController = SplitViewRouter().baseViewController
            windowController.window?.makeKeyAndOrderFront(nil)
            windowController.window?.makeMain()
        }
        return true
    }
    
    
    
}


