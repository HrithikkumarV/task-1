//
//  SearchTableAssembler.swift
//  Task-2 - MultiplatformMode
//
//  Created by Hrithik Kumar V on 22/06/22.
//

import Foundation
class SearchTableAssembler{
    static func getBaseViewController(router : SearchTableRouterProtocol) -> SearchTableViewControllerProtocol{
        let viewController = SearchTableViewController()
        let presenter = SearchTablePresenter()
        presenter.router = router
        presenter.viewController = viewController
        viewController.presenter = presenter
        return viewController
    }
    
}
