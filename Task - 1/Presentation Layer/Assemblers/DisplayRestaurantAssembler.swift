//
//  DisplayRestaurantAssembler.swift
//  Task-2 - MultiplatformMode
//
//  Created by Hrithik Kumar V on 22/06/22.
//

import Foundation
class DisplayRestaurantsAssembler{
    
    static func getBaseViewController(router : DisplayRestaurantsRouterProtocol) -> DisplayRestaurantsCollectionViewControllerProtocol{
        let viewController = DisplayRestaurantsCollectionViewController()
        let presenter = DisplayRestaurantsPresenter()
        presenter.router = router
        presenter.viewController = viewController
        viewController.presenter = presenter
        return viewController
    }
    
}
