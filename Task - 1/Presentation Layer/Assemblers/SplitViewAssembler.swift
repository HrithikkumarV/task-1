//
//  MultiPlatformModeAssembler.swift
//  Task-2 - MultiplatformMode
//
//  Created by Hrithik Kumar V on 22/06/22.
//

import Cocoa

class MultiPlatformModeAssembler{
    static func getBaseViewController(router : SplitViewRouterProtocol) -> SplitViewControllerProtocol{
        let viewController = SplitViewController()
        let presenter = SplitViewPresenter()
        presenter.router = router
        presenter.viewController = viewController
        viewController.presenter = presenter
       
        return viewController
    }
    
    static func getBaseWindowController(router : SplitViewRouterProtocol) -> MainWindowControllerProtocol{
        let windowConroller = MainWindowController()
        windowConroller.router = router
        return windowConroller
    }
    
    
}
