//
//  DisplayRestaurantPresenter.swift
//  Task-2 - MultiplatformMode
//
//  Created by Hrithik Kumar V on 22/06/22.
//

import Cocoa

class DisplayRestaurantsPresenter : DisplayRestaurantsPresenterProtocol{
    
    var router : DisplayRestaurantsRouterProtocol?
    weak var viewController : DisplayRestaurantsCollectionViewControllerProtocol?
    
   
    
    
}

protocol DisplayRestaurantsPresenterProtocol : AnyObject{
    var router : DisplayRestaurantsRouterProtocol? {get set}
    var viewController : DisplayRestaurantsCollectionViewControllerProtocol? {get set}
}
