//
//  SearchTablePresenter.swift
//  Task-2 - MultiplatformMode
//
//  Created by Hrithik Kumar V on 22/06/22.
//

import Cocoa

class SearchTablePresenter : SearchTablePresenterProtocol{
    
    var router : SearchTableRouterProtocol?
    weak var viewController : SearchTableViewControllerProtocol?
    
   
    
    func didTapClearHistory() {
        router?.addDisplayRestaurants()
    }
   

}

protocol SearchTablePresenterProtocol : AnyObject{
    var router : SearchTableRouterProtocol? {get set}
    var viewController : SearchTableViewControllerProtocol? {get set}
    func didTapClearHistory()
}
