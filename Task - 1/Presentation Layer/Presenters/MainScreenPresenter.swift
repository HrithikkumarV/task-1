//
//  MainScreenPresenter.swift
//  Task-2 - MultiplatformMode
//
//  Created by Hrithik Kumar V on 22/06/22.
//

import Cocoa

class MainScreenPresenter : MainScreenPresenterProtocol{
    
    var router : MainScreenRouterProtocol?
    weak var viewController : MainViewControllerProtocol?
    
   
    
    func didTapUserButton() {

        router?.addSearchTable()
        
    }
    
    func didTapRestaurantButton() {

        router?.addSearchTable()
    }
}

protocol MainScreenPresenterProtocol : AnyObject{
    var router : MainScreenRouterProtocol? {get set}
    var viewController : MainViewControllerProtocol? {get set}
    func didTapUserButton()
    func didTapRestaurantButton()
}
