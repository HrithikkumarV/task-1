//
//  MultiPlatformModePresenter.swift
//  Task-2 - MultiplatformMode
//
//  Created by Hrithik Kumar V on 22/06/22.
//

import Cocoa

class SplitViewPresenter : SplitViewPresenterProtocol{
    
    var router : SplitViewRouterProtocol?
    weak var viewController : SplitViewControllerProtocol?
    
   
    func addSideBarViewController(viewController : NSViewController){
        self.viewController?.addSideBarViewController(viewController: viewController)
        
    }
    
    func addContentListViewController(viewController : NSViewController){
        
        self.viewController?.addContentListViewController(viewController: viewController)
    }
    
    func addDetailViewController(viewController : NSViewController){
        self.viewController?.addDetailViewController(viewController: viewController)
    }
}

protocol SplitViewPresenterProtocol : AnyObject{
    
    var router : SplitViewRouterProtocol? {get set}
    
    var viewController : SplitViewControllerProtocol? {get set}
    
    func addSideBarViewController(viewController : NSViewController)
    
    
    func addContentListViewController(viewController : NSViewController)
    
    
    func addDetailViewController(viewController : NSViewController)
    
  
    
}
