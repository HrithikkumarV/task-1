//
//  MainWindowRouterProtocol.swift
//  Task-2 - MultiplatformMode
//
//  Created by Hrithik Kumar V on 22/06/22.
//

import Foundation

protocol MainWindowRouterProtocol : AnyObject{
    var mainWindowController :  MainWindowControllerProtocol? { get set }

}
