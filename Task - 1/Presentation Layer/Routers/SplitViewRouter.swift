//
//  MultiPlatFormModeRouter.swift
//  Task-2 - MultiplatformMode
//
//  Created by Hrithik Kumar V on 22/06/22.
//

import Cocoa

class SplitViewRouter : SplitViewRouterProtocol{
    var mainWindowController: MainWindowControllerProtocol?

    var baseViewController  : SplitViewControllerProtocol?

    var mainScreenBaseViewController: MainViewControllerProtocol?
    
    var searchTableBaseViewController: SearchTableViewControllerProtocol?
   
    var displayRestaurantsbaseViewController: DisplayRestaurantsCollectionViewControllerProtocol?
    
    init(){
        mainWindowController = MultiPlatformModeAssembler.getBaseWindowController(router: self)
        baseViewController = MultiPlatformModeAssembler.getBaseViewController(router: self)
        addMainScreen()
    }
    
  
    func addMainScreen() {
        mainScreenBaseViewController = MainScreenAssembler.getBaseViewController(router: self)
        baseViewController?.presenter?.addSideBarViewController(viewController: mainScreenBaseViewController!)
    }
    
    func addSearchTable() {
        searchTableBaseViewController = SearchTableAssembler.getBaseViewController(router: self)
        baseViewController?.presenter?.addContentListViewController(viewController: searchTableBaseViewController!)
    }
    
    func addDisplayRestaurants() {
        displayRestaurantsbaseViewController = DisplayRestaurantsAssembler.getBaseViewController(router: self)
        baseViewController?.presenter?.addDetailViewController(viewController: displayRestaurantsbaseViewController!)
    }
    
    
    
}

protocol SplitViewRouterProtocol : DisplayRestaurantsRouterProtocol,SearchTableRouterProtocol,MainScreenRouterProtocol,MainWindowRouterProtocol{
    var baseViewController  : SplitViewControllerProtocol? {get set}
    func addMainScreen()
}





