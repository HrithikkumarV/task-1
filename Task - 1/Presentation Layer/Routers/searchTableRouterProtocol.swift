//
//  searchTableRouterProtocol.swift
//  Task-2 - MultiplatformMode
//
//  Created by Hrithik Kumar V on 22/06/22.
//

import Foundation
protocol SearchTableRouterProtocol : AnyObject{
    var searchTableBaseViewController  : SearchTableViewControllerProtocol? {get set}
   
    func addDisplayRestaurants()
}
