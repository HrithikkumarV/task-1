//
//  MainScreenRouterProtocol.swift
//  Task-2 - MultiplatformMode
//
//  Created by Hrithik Kumar V on 22/06/22.
//

import Foundation

protocol MainScreenRouterProtocol : AnyObject{
    var mainScreenBaseViewController  : MainViewControllerProtocol? {get set}
    func addSearchTable()
    
}
