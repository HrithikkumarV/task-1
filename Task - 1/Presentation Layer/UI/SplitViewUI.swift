//
//  MultiPlatformModeUI.swift
//  Task-2 - MultiplatformMode
//
//  Created by Hrithik Kumar V on 22/06/22.
//

import Cocoa


class SplitViewController : NSSplitViewController,SplitViewControllerProtocol{
    
    var presenter: SplitViewPresenterProtocol?
    
    var secondSplitItemIsVertical : Bool?
    
    var sidebarSplitItem : NSSplitViewItem?
        
    var contentListSplitViewItem : NSSplitViewItem?
        
    var detailViewSplitItem : NSSplitViewItem?

    var contentSplitViewController : NSSplitViewController = {
        let contentSplitViewController = NSSplitViewController()
        return contentSplitViewController
    }()

    lazy var contentSplitViewControllerSplitItem : NSSplitViewItem = {
        let contentSplitViewController = contentSplitViewController
        contentSplitViewController.splitView.isVertical = true
        let contentSplitViewControllerSplitItem = NSSplitViewItem(contentListWithViewController: contentSplitViewController)
        contentSplitViewControllerSplitItem.minimumThickness = 600
        return contentSplitViewControllerSplitItem
    }()
    
  
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        splitView.dividerStyle = .thin

        self.splitView.postsFrameChangedNotifications = true
        NotificationCenter.default.addObserver(self, selector: #selector(didChangeFrame), name: NSSplitView.frameDidChangeNotification, object: splitView)

        
    }
    
    
   
    
    
    override func splitViewWillResizeSubviews(_ notification: Notification) {
        findOrientation()
    }
    
    func findOrientation(){
        if((secondSplitItemIsVertical == nil || secondSplitItemIsVertical != true) && splitView.frame.size.width > 1000){
            
            contentSplitViewController.splitView.isVertical = true
            contentSplitViewController.splitView.setPosition(splitView.frame.width/2, ofDividerAt: 0)
            
        }
        else if((secondSplitItemIsVertical == nil || secondSplitItemIsVertical != false) && splitView.frame.size.width <= 1000){
            contentSplitViewController.splitView.isVertical = false
            contentSplitViewController.splitView.setPosition(splitView.frame.height/2, ofDividerAt: 0)
 
        }
    }
    
   

    func addSideBarViewController(viewController : NSViewController){
        if(sidebarSplitItem == nil){
           
                sidebarSplitItem = NSSplitViewItem(sidebarWithViewController: viewController)
                sidebarSplitItem?.minimumThickness = 300
                sidebarSplitItem?.maximumThickness = 300
                sidebarSplitItem?.canCollapse = false
                addSplitViewItem(sidebarSplitItem!)
        }
    }
    
    func addContentListViewController(viewController : NSViewController){
        if(contentListSplitViewItem == nil){
            addSplitViewItem(contentSplitViewControllerSplitItem)
            contentListSplitViewItem =  NSSplitViewItem(contentListWithViewController: viewController)
            contentListSplitViewItem?.minimumThickness = 300
            contentListSplitViewItem?.canCollapse = false
            contentSplitViewController.addSplitViewItem(contentListSplitViewItem!)
            findOrientation()
        }
       
    }
    
    func addDetailViewController(viewController : NSViewController){
        if(detailViewSplitItem == nil){
            detailViewSplitItem = NSSplitViewItem(viewController: viewController)
            detailViewSplitItem?.minimumThickness = 300
            detailViewSplitItem?.canCollapse = false
            contentSplitViewController.addSplitViewItem(detailViewSplitItem!)
            findOrientation()
        }
       
    }

    
   
    
    
   
    

    @objc func didChangeFrame(){
      
    }
    
    
    
   
}




protocol SplitViewControllerProtocol : NSSplitViewController{
    var presenter :  SplitViewPresenterProtocol? { get set }
    
    func addSideBarViewController(viewController : NSViewController)
    
    
    func addContentListViewController(viewController : NSViewController)
    
    
    func addDetailViewController(viewController : NSViewController)
    
    
}

