//
//  DisplayRestaurantsView.swift
//  Task-2 - MultiplatformMode
//
//  Created by Hrithik Kumar V on 22/06/22.
//

import Cocoa
class DisplayRestaurantsCollectionViewItem : NSCollectionViewItem{
    
    static let cellIdentifier = "DisplayRestaurantsCollectionViewItem"
    
    let theme : Theme = Theme()
    
    let restaurantDisplayCollectionViewItemView : RestaurantDisplayCollectionViewItemView = {
        let restaurantDisplayCollectionViewItemView = RestaurantDisplayCollectionViewItemView()
        restaurantDisplayCollectionViewItemView.wantsLayer = true
        restaurantDisplayCollectionViewItemView.translatesAutoresizingMaskIntoConstraints = false
        return restaurantDisplayCollectionViewItemView
    }()
    
    override func loadView() {
        view = NSView()
        view.wantsLayer = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addRestaurantDisplayCollectionViewItemView()
        applyTheme()
        addTrackingArea()
    }
    
    override func mouseEntered(with event: NSEvent) {
        self.view.layer?.backgroundColor = theme.mouseEnteredHighlightColour
    }
        

    override func mouseExited(with event: NSEvent) {
        self.view.layer?.backgroundColor = theme.mouseExitedHighlightColour
    }
    
    func applyTheme(){
        self.view.layer?.backgroundColor = theme.backgroundColor
    }
    
    private func addTrackingArea(){
        self.view.addTrackingArea(NSTrackingArea(rect: self.view.frame, options: [.inVisibleRect, .activeAlways, .mouseEnteredAndExited], owner: self, userInfo: nil))
    }
    private func addRestaurantDisplayCollectionViewItemView(){
        self.view.addSubview(restaurantDisplayCollectionViewItemView)
        NSLayoutConstraint.activate([
            restaurantDisplayCollectionViewItemView.topAnchor.constraint(equalTo: self.view.topAnchor ),
            restaurantDisplayCollectionViewItemView.leftAnchor.constraint(equalTo: self.view.leftAnchor),
            restaurantDisplayCollectionViewItemView.rightAnchor.constraint(equalTo: self.view.rightAnchor),
            restaurantDisplayCollectionViewItemView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor)
        ])
    }
    
    struct Theme{
        var mouseEnteredHighlightColour : CGColor = NSColor.highlightColor.cgColor
        var mouseExitedHighlightColour : CGColor = NSColor.clear.cgColor
        var backgroundColor : CGColor = .clear
    }
    
    
}


class RestaurantDisplayCollectionViewItemView : NSView{
   
    private let padding : CGFloat = 5
    
    let theme : Theme = Theme()
    
    private let restaurantNameCellLabel : NSTextField = {
        let  restaurantNameCellLabel = NSTextField()
        restaurantNameCellLabel.wantsLayer = true
        restaurantNameCellLabel.isEditable = false
        restaurantNameCellLabel.translatesAutoresizingMaskIntoConstraints = false
        restaurantNameCellLabel.alignment = .left
        restaurantNameCellLabel.isBordered = false
        restaurantNameCellLabel.isBezeled = false
        restaurantNameCellLabel.isSelectable = true
        restaurantNameCellLabel.sizeToFit()
        restaurantNameCellLabel.lineBreakMode = .byWordWrapping
        restaurantNameCellLabel.setContentHuggingPriority(.fittingSizeCompression, for: .horizontal)
        restaurantNameCellLabel.setContentCompressionResistancePriority(.fittingSizeCompression, for: .horizontal)
        return restaurantNameCellLabel
    }()
    
    
    private let restaurantCuisineCellLabel : NSTextField = {
        let  restaurantCuisineCellLabel = NSTextField()
        restaurantCuisineCellLabel.wantsLayer = true
        restaurantCuisineCellLabel.isEditable = false
        restaurantCuisineCellLabel.translatesAutoresizingMaskIntoConstraints = false
        restaurantCuisineCellLabel.alignment = .left
        restaurantCuisineCellLabel.isBordered = false
        restaurantCuisineCellLabel.isBezeled = false
        restaurantCuisineCellLabel.isSelectable = true
        restaurantCuisineCellLabel.sizeToFit()
        restaurantCuisineCellLabel.lineBreakMode = .byWordWrapping
        restaurantCuisineCellLabel.setContentHuggingPriority(.fittingSizeCompression, for: .horizontal)
        restaurantCuisineCellLabel.setContentCompressionResistancePriority(.fittingSizeCompression, for: .horizontal)
        return restaurantCuisineCellLabel
    }()
    
    private let restaurantStarRatingCellLabel : NSTextField = {
        let  restaurantStarRatingCellLabel = NSTextField()
        restaurantStarRatingCellLabel.wantsLayer = true
        restaurantStarRatingCellLabel.isEditable = false
        restaurantStarRatingCellLabel.translatesAutoresizingMaskIntoConstraints = false
        restaurantStarRatingCellLabel.alignment = .left
        restaurantStarRatingCellLabel.isBordered = false
        restaurantStarRatingCellLabel.isBezeled = false
        restaurantStarRatingCellLabel.isSelectable = true
        restaurantStarRatingCellLabel.sizeToFit()
        restaurantStarRatingCellLabel.lineBreakMode = .byWordWrapping
        restaurantStarRatingCellLabel.setContentHuggingPriority(.fittingSizeCompression, for: .horizontal)
        restaurantStarRatingCellLabel.setContentCompressionResistancePriority(.fittingSizeCompression, for: .horizontal)
        return restaurantStarRatingCellLabel
    }()
    
   
    private let availableCellLabel : NSTextField = {
        let  availableCellLabel = NSTextField()
        availableCellLabel.wantsLayer = true
        availableCellLabel.isEditable = false
        availableCellLabel.translatesAutoresizingMaskIntoConstraints = false
        availableCellLabel.alignment = .right
        availableCellLabel.isBordered = false
        availableCellLabel.isBezeled = false
        availableCellLabel.isSelectable = true
        availableCellLabel.sizeToFit()
        availableCellLabel.lineBreakMode = .byWordWrapping
        availableCellLabel.setContentHuggingPriority(.fittingSizeCompression, for: .horizontal)
        availableCellLabel.setContentCompressionResistancePriority(.fittingSizeCompression, for: .horizontal)
        return availableCellLabel
    }()
    
   
    
    
    private let restaurantProfileImageCellImageView : NSImageView  = {
        let  restaurantProfileImageCellImageView = NSImageView()
        restaurantProfileImageCellImageView.translatesAutoresizingMaskIntoConstraints = false
        restaurantProfileImageCellImageView.wantsLayer = true
        restaurantProfileImageCellImageView.imageAlignment = .alignCenter
        restaurantProfileImageCellImageView.imageScaling = .scaleAxesIndependently
        return restaurantProfileImageCellImageView
    }()
    
   
    var contentView : NSView = {
        let  contentView = NSView()
        contentView.wantsLayer = true
        contentView.layer?.cornerRadius = 10
        contentView.translatesAutoresizingMaskIntoConstraints = false
        return contentView
    }()
    
    init(){
        super.init(frame: .zero)
        self.addSubview(contentView)
        initialiseViewElements()
        applyTheme()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    override func prepareForReuse() {
        super.prepareForReuse()
        restaurantProfileImageCellImageView.image = nil
        restaurantNameCellLabel.stringValue = ""
        restaurantStarRatingCellLabel.stringValue = ""
        availableCellLabel.stringValue = ""
        restaurantCuisineCellLabel.stringValue = ""
        
    }
    
    
    func configureContent(restaurentCellContents : (restaurantProfileImage : Data , restaurantName : String , restaurantStarRating : String , restaurantCuisine : String , restaurantLocality : String , deliveryTiming : Int , restaurantOpensNextAt  : String,restaurantIsAvailable : Int,isDeliverable : Bool)){
        restaurantNameCellLabel.stringValue = restaurentCellContents.restaurantName
        restaurantCuisineCellLabel.stringValue = restaurentCellContents.restaurantCuisine
        restaurantStarRatingCellLabel.stringValue =  String(restaurentCellContents.restaurantStarRating.prefix(upTo: restaurentCellContents.restaurantStarRating.index(restaurentCellContents.restaurantStarRating.startIndex, offsetBy: 3)))
       
        restaurantProfileImageCellImageView.image = NSImage(data: restaurentCellContents.restaurantProfileImage)
        updateRestaurantStatus(deliveryTiming : restaurentCellContents.deliveryTiming, isDeliverable : restaurentCellContents.isDeliverable , restaurantIsAvailable : restaurentCellContents.restaurantIsAvailable , restaurantOpensNextAt : restaurentCellContents.restaurantOpensNextAt)
    }
    
    
    
    func applyTheme(){
        contentView.layer?.backgroundColor = theme.contentViewBackgroundColor
        restaurantProfileImageCellImageView.layer?.backgroundColor = theme.restaurantProfileImageCellImageViewBackgroundColor
        restaurantNameCellLabel.backgroundColor = theme.restaurantNameCellLabelBackgroundColor
        restaurantCuisineCellLabel.backgroundColor = theme.restaurantCuisineCellLabelBackgroundColor
        restaurantStarRatingCellLabel.backgroundColor = theme.restaurantStarRatingCellLabelBackgroundColor
        availableCellLabel.backgroundColor = theme.availableCellLabelBackgroundColor
        restaurantNameCellLabel.font = theme.restaurantNameCellLabelFont
        restaurantCuisineCellLabel.font = theme.restaurantCuisineCellLabelFont
        restaurantStarRatingCellLabel.font = theme.restaurantStarRatingCellLabelFont
        availableCellLabel.font = theme.availableCellLabelFont
        restaurantNameCellLabel.textColor = theme.restaurantNameCellLabelTextColor
        restaurantCuisineCellLabel.textColor = theme.restaurantCuisineCellLabelTextColor
        restaurantStarRatingCellLabel.textColor = theme.restaurantStarRatingCellLabelTextColor
        availableCellLabel.textColor = theme.availableCellLabelTextColor
    }
    
    
    
    
    private func initialiseViewElements(){
        addContentView()
       addRestaurantProfileImageCellImageView()
       addRestaurantNameCellLabel()
        getRestaurantCuisineCellLabelContraints()
       addRestaurantStarRatingCellLabel()
       addAvailableCellLabel()
       
    }
    
    private func updateRestaurantStatus(deliveryTiming : Int, isDeliverable : Bool , restaurantIsAvailable : Int , restaurantOpensNextAt : String){
        if(!isDeliverable){
            availableCellLabel.stringValue = "Not Deliverable"
            
        }
        else if(restaurantIsAvailable == 1){
            availableCellLabel.stringValue = "\(deliveryTiming) mins"
           
        }
        else{
            availableCellLabel.stringValue = "CLOSED"
            
        }
    }
    
  
    
    private func addContentView(){
        self.addSubview(contentView)
        NSLayoutConstraint.activate([
            contentView.topAnchor.constraint(equalTo: self.topAnchor),
            contentView.leftAnchor.constraint(equalTo: self.leftAnchor),
            contentView.rightAnchor.constraint(equalTo:self.rightAnchor),
            ])
    }
   
    
    private func addRestaurantProfileImageCellImageView(){
        contentView.addSubview(restaurantProfileImageCellImageView)
        NSLayoutConstraint.activate([
            restaurantProfileImageCellImageView.topAnchor.constraint(equalTo: contentView.topAnchor , constant: padding),
            restaurantProfileImageCellImageView.leftAnchor.constraint(equalTo: contentView.leftAnchor , constant: padding),
            restaurantProfileImageCellImageView.rightAnchor.constraint(equalTo: contentView.rightAnchor,constant: -padding),
            restaurantProfileImageCellImageView.heightAnchor.constraint(equalToConstant: 150)
        ])
       
        
    }
    
    
    private func addRestaurantNameCellLabel(){
        contentView.addSubview(restaurantNameCellLabel)
        NSLayoutConstraint.activate([
            restaurantNameCellLabel.topAnchor.constraint(equalTo: restaurantProfileImageCellImageView.bottomAnchor ,constant: padding),
                                    
            restaurantNameCellLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor,constant: padding),
                                     
            restaurantNameCellLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor , constant: -padding),
        ])
        
        
    }
    
    
    
    private func getRestaurantCuisineCellLabelContraints(){
        contentView.addSubview(restaurantCuisineCellLabel)
        NSLayoutConstraint.activate([
            restaurantCuisineCellLabel.topAnchor.constraint(equalTo: restaurantNameCellLabel.bottomAnchor ,constant: padding),
            restaurantCuisineCellLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor , constant: padding),
            restaurantCuisineCellLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor , constant: -padding),
            restaurantCuisineCellLabel.heightAnchor.constraint(equalToConstant: 20)])
        
       
    }
   
    private func addRestaurantStarRatingCellLabel(){
        contentView.addSubview(restaurantStarRatingCellLabel)
        NSLayoutConstraint.activate([
            restaurantStarRatingCellLabel.topAnchor.constraint(equalTo: restaurantCuisineCellLabel.bottomAnchor,constant: padding),
            restaurantStarRatingCellLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor,constant: padding),
            restaurantStarRatingCellLabel.rightAnchor.constraint(equalTo: contentView.centerXAnchor),
            ])
        
       
    }
    
    private func addAvailableCellLabel(){
        contentView.addSubview(availableCellLabel)
        NSLayoutConstraint.activate([
            availableCellLabel.topAnchor.constraint(equalTo: restaurantCuisineCellLabel.bottomAnchor,constant: padding),
            availableCellLabel.leftAnchor.constraint(equalTo: contentView.centerXAnchor),
            availableCellLabel.rightAnchor.constraint(equalTo : contentView.rightAnchor,constant: -padding),
            contentView.bottomAnchor.constraint(equalTo: availableCellLabel.bottomAnchor,constant: padding)
            ])
        
    }
    
    struct Theme {
        var contentViewBackgroundColor : CGColor = .clear
        var restaurantProfileImageCellImageViewBackgroundColor : CGColor = .clear
        var restaurantNameCellLabelBackgroundColor : NSColor = .clear
        var restaurantCuisineCellLabelBackgroundColor : NSColor = .clear
        var restaurantStarRatingCellLabelBackgroundColor : NSColor = .clear
        var availableCellLabelBackgroundColor : NSColor = .clear
        var restaurantNameCellLabelFont : NSFont = NSFont.systemFont(ofSize: 20, weight: .medium)
        var restaurantCuisineCellLabelFont : NSFont = NSFont.systemFont(ofSize: 10, weight: .medium)
        var restaurantStarRatingCellLabelFont : NSFont = NSFont.systemFont(ofSize: 15, weight: .medium)
        var availableCellLabelFont : NSFont = NSFont.systemFont(ofSize: 15, weight: .medium)
        var restaurantNameCellLabelTextColor : NSColor = .white
        var restaurantCuisineCellLabelTextColor : NSColor = .white
        var restaurantStarRatingCellLabelTextColor : NSColor = .white
        var availableCellLabelTextColor : NSColor = .white
    }
    
}


