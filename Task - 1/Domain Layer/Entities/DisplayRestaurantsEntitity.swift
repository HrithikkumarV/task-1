//
//  DisplayRestaurantsEntitity.swift
//  Task - 1
//
//  Created by Hrithik Kumar V on 23/06/22.
//

import Foundation

struct RestaurantCompleteDetails {
     var restaurantDetails : RestaurantDetails = RestaurantDetails()
     var restaurantId : Int = 0
     var restaurantStarRating : String = ""
     var totalNumberOfRating : Int = 0
     var restaurantAddress : AddressDetails = AddressDetails()
     var restaurantOpensNextAt : String = ""
     var restaurantIsAvailable : Int = 0
     var restaurantAccountStatus : Int = 0
     var isDeliverable : Bool = false
     var restaurantFoodPackagingCharges : Int = 0
}
