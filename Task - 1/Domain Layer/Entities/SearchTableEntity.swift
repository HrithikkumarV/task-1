//
//  SearchUseCase.swift
//  Task-2 - MultiplatformMode
//
//  Created by Hrithik Kumar V on 22/06/22.
//

import Foundation


struct SearchModel {
    var searchItemName : String = ""
    var searchItemType : SearchItemType = .Search
    var searchType : SearchType = .searchSuggestionItem
}
